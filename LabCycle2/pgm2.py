def remove_vowels(str):
    str = str.replace("a", "")
    str = str.replace("e", "")
    str = str.replace("i", "")
    str = str.replace("o", "")
    str = str.replace("u", "")
    str = str.replace("A", "")
    str = str.replace("E", "")
    str = str.replace("I", "")
    str = str.replace("O", "")
    str = str.replace("U", "")
    return str

str = input("Enter a string: ")
print("String after removing vowels from '",str,"' is:",remove_vowels(str))