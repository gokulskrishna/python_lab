def positive_list(list):
    return [i for i in list if i>0] 

def square_list(list):
    return [i**2 for i in list] 

def vowel_list(word):
    return [i for i in word if i in "aeiouAEIOU"] 

def ordinal_list(word):
    return [ord(i) for i in word] 

print("Enter a list of numbers to generate positive list seperated by spaces: ")
list = [int(i) for i in input().split()]
print("Positive list: ", positive_list(list))
print("Enter a list of numbers to generate square list seperated by spaces: ")
list = [int(i) for i in input().split()] 
print("Square list: ", square_list(list))
print("Enter a word to generate vowel list: ")
word = input()
print("Vowel list: ", vowel_list(word))

print("Enter a word to generate ordinal list: ")
word = input()
print("Ordinal list: ", ordinal_list(word))